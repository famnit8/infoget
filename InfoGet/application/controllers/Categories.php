<?php
class Categories extends CI_Controller{


	public function index($offset = 0){

		

		// Pagination Config

			$config['base_url'] = base_url() . 'categories';
			$config['total_rows'] = $this->db->count_all('posts');
			$config['per_page'] = 3;
			$config['uri_segment'] = 3;
			$config['attributes'] = array('class' => 'pagination-link');

			// Init Pagination 
			$this->pagination->initialize($config);


		$data['title'] = 'Categories';

		//$data['posts'] = $this->post_model->get_posts(FALSE, $config['per_page'], $offset);

		$data['categories'] = $this->category_model->get_categories(FALSE, $config['per_page'], $offset);

		

		$this->load->view('templates/header');
		$this->load->view('categories/index', $data);
		$this->load->view('templates/footer');

	}

	public function create(){
		//Check login
			if(!$this->session->userdata('logged_in')){
				redirect('users/login');
			}
			
		$data['title'] = 'Create Category';


		$this->form_validation->set_rules('name', 'Name', 'required');

		if($this->form_validation->run() === FALSE){
			$this->load->view('templates/header');
			$this->load->view('categories/create', $data);
			$this->load->view('templates/footer');
		}else{
			$this->category_model->create_category();

			//Set message
			$this->session->set_flashdata('category_created', 'Your category has been created.');

			redirect('categories');
		}
	}

	public function posts($id){

		// Pagination Config


			$one_category_posts = $this->post_model->get_posts_by_category($id);

			

		$data['title'] = $this->category_model->get_category($id)->name;

		$data['posts'] = $one_category_posts;

		
		$this->load->view('templates/header');
		$this->load->view('posts/index', $data);
		$this->load->view('templates/footer');
	}


		public function delete($id){

			//Check login
			if(!$this->session->userdata('logged_in')){
				redirect('users/login');
			}

			$this->category_model->delete_category($id);

			//Set message
			$this->session->set_flashdata('category_deleted', 'Your category has been deleted.');

			redirect('categories');
		}
}