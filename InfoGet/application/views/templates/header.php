<html>
	<head>
		<title>Info-Get</title>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style2.css">
		<link rel="stylesheet" href="https://bootswatch.com/5/flatly/bootstrap.css"> 
		<link rel="stylesheet" href="https://bootswatch.com/_vendor/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		
		<script src="https://cdn.ckeditor.com/4.19.1/standard/ckeditor.js"></script>
	

	</head>

	
<body>


	<div class="sidenav">
	  
	  	<a class="navbar-logo" href="<?php echo base_url(); ?> "><strong>Info-Get</strong></a>
		<a class="nav-link active nav-items nav-posts " href="<?php echo base_url(); ?>posts ">Posts</a>				
		<a class="nav-link active nav-items" href="<?php echo base_url(); ?>categories">Categories</a>
		<a class="nav-link active nav-items" href="<?php echo base_url(); ?>about">About</a>
		<a class="nav-link active nav-items" href="<?php echo base_url(); ?>contact">Contact</a>
						
		<div class="nav-bottom">
			<?php if(!$this->session->userdata('logged_in')) : ?>

								
				<a class="nav-link active nav-items" href="<?php echo base_url(); ?>users/login">Login</a>						
				<a  class="nav-link active nav-items" href="<?php echo base_url(); ?>users/register">Register</a>
						

			<?php endif;  ?>

			<?php if($this->session->userdata('logged_in')) : ?>

					<a class="nav-link active nav-items" href="<?php echo base_url(); ?>posts/create ">Create Post</a>
					<a class="nav-link active nav-items" href="<?php echo base_url(); ?>categories/create">Create Category</a>
					<a class="nav-link active nav-items" href="<?php echo base_url(); ?>users/logout">Logout</a>
			
			<?php endif; ?>

		</div>
	</div>


<div class="container main-container">

	<!-- Flash messages -->
	<?php if($this->session->flashdata('user_registered')): ?>
		<?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_registered').'</p>'; ?>
	<?php endif; ?>

	<?php if($this->session->flashdata('post_created')): ?>
		<?php echo '<p class="alert alert-success">'.$this->session->flashdata('post_created').'</p>'; ?>
	<?php endif; ?>

	<?php if($this->session->flashdata('post_updated')): ?>
		<?php echo '<p class="alert alert-success">'.$this->session->flashdata('post_updated').'</p>'; ?>
	<?php endif; ?>

	<?php if($this->session->flashdata('category_created')): ?>
		<?php echo '<p class="alert alert-success">'.$this->session->flashdata('category_created').'</p>'; ?>
	<?php endif; ?>

	<?php if($this->session->flashdata('post_deleted')): ?>
		<?php echo '<p class="alert alert-danger">'.$this->session->flashdata('post_deleted').'</p>'; ?>
	<?php endif; ?>

	<?php if($this->session->flashdata('login_failed')): ?>
		<?php echo '<p class="alert alert-danger">'.$this->session->flashdata('login_failed').'</p>'; ?>
	<?php endif; ?>

	<?php if($this->session->flashdata('user_loggedin')): ?>
		<?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_loggedin').'</p>'; ?>
	<?php endif; ?>

	<?php if($this->session->flashdata('user_loggedout')): ?>
		<?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_loggedout').'</p>'; ?> 
	<?php endif; ?>

	<?php if($this->session->flashdata('category_deleted')): ?>
		<?php echo '<p class="alert alert-danger">'.$this->session->flashdata('category_deleted').'</p>'; ?>
	<?php endif; ?>






			