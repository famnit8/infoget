<h2 class="text-center read-more-title"><strong><?= $title ?></strong></h2>

<?php echo validation_errors(); ?>



<div class="container-fluid">
    <div class="row d-flex">
        <div class="row shadow p-3 mb-4 bg-body rounded col-md-11 edit-block">
          <?php echo form_open('posts/update'); ?>
          <input type="hidden" name="id" value="<?php echo $post['id']; ?>">
          <div class="form-group">
            <label><strong>Title</strong></label>
            <input type="text" class="form-control"  name="title" placeholder="Add Title" value="<?php echo $post['title']; ?>"> 
          </div>
          
          <div class="form-group">
            <label><strong>Body</strong></label>
            <textarea id="editor1" class="form-control" name="body" placeholder="Add Body"><?php echo $post['body']; ?> </textarea>
          </div>
          
          <div class="form-group">
            <label class="post-label"><strong>Category</strong></label>
            <select name="category_id" class="form-control">
              <?php foreach($categories as $category): ?>
                <option value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?>
                </option> 
              <?php endforeach; ?>
            </select>
          </div>
        
          <button type="submit" class="btn btn-lg btn-dark">Submit</button>
        </form>


          
        </div>    
  </div>
</div>