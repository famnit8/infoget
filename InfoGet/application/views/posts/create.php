<h2 class="text-center read-more-title"><strong><?= $title ?></strong></h2>



<div class="container-fluid">
    <div class="row d-flex">
        <div class="row shadow p-3 mb-4 bg-body rounded col-md-11 edit-block">
          <?php echo validation_errors(); ?>

          <?php echo form_open_multipart('posts/create'); ?>
            <div class="form-group">
              <label>Title</label>
              <input type="text" class="form-control"  name="title" placeholder="Add Title">
            </div>
            <div class="form-group">

              <label>Body</label>
              <textarea id="editor1" class="form-control" name="body" placeholder="Add Body"> </textarea>
              
            </div>
            
            <div class="form-group">
              <label class="category-label">Category</label>
              <select name="category_id" class="form-control">
                <?php foreach($categories as $category): ?>
                  <option value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?>
                  </option> 
                <?php endforeach; ?>
              </select>
            </div>

            <div class="form-group">
                <label>Upload Image</label>
                <input type="file" name="userfile" size="20"> 
            </div>

            <button type="submit" class="btn btn-lg btn-dark">Submit</button>
          </form>
            
       

        </div>    
  </div>
</div>