<h2 class="text-center read-more-title"><strong><?= $title ?></strong></h2>

<small class="post-date">Posted on: <?php echo $post['created_at']; ?></small><br>




<div class="container-fluid">
    <div class="row d-flex">
        <div class="row shadow p-3 mb-4 bg-body rounded col-md-11 posts-blocks">

           	<img  src="<?php echo site_url(); ?>assets/images/posts/<?php echo $post['post_image']; ?>">

			<div class="post-body">
				<?php echo $post['body']; ?>
			</div>

			<?php if($this->session->userdata('user_id') == $post['user_id']): ?>
				<hr>
				
				<?php echo form_open('/posts/delete/'.$post['id']); ?>
					
					<a class="btn  btn-lg btn-dark" href="edit/<?php echo $post['slug']; ?>">Edit</a>
					<input type="submit" value="Delete" class="btn btn-lg btn-danger">

				</form>
			<?php endif; ?>

        </div>	  
	</div>
</div>

<hr>

<div class="container-fluid">
    <div class="row d-flex">
        <div class="row shadow p-3 mb-4 bg-body rounded col-md-11 posts-blocks">

           	
        	<h3>Comments</h3>
			<?php if($comments) : ?>
				<?php foreach($comments as $comment) : ?>
					<div class="alert alert-dismissible alert-secondary">
						<h6><?php echo $comment['body']; ?> [by <strong><?php echo $comment['name']; ?></strong>]</h6>
					</div>
				<?php endforeach; ?>
			<?php else :?>
				<p>No Comments To Display</p>
			<?php endif; ?>

        </div>	  
	</div>
</div>


<hr>
	
<div class="container-fluid">
    <div class="row d-flex">
        <div class="row shadow p-3 mb-4 bg-body rounded col-md-11 posts-blocks">
			<h3>Add Comment</h3>
           	<?php echo validation_errors(); ?>
			<?php echo form_open('comments/create/'.$post['id']); ?>
				<div class="form-group">
					<label>Name</label>
					<input type="text" name="name" class="form-control">
				</div>
				<div class="form-group">
					<label>Email</label>
					<input type="email" name="email" class="form-control">
				</div>
				<div class="form-group">
					<label>Body</label>
					<textarea name="body" class="form-control" rule="required"></textarea>
				</div>
				<input type="hidden" name="slug" value="<?php echo $post['slug']; ?>">
				<button class="btn btn-lg btn-primary" type="submit">Submit</button>
        </div>	  
	</div>
</div>


</form>