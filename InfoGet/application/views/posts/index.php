<h2 class="text-center all-titles"><strong><?= $title ?></strong></h2>


<?php foreach($posts as $post) : ?>

		
<div class="container-fluid">
    <div class="row text-center d-flex">
        <div class="row shadow p-3 mb-4 bg-body rounded col-md-11 posts-blocks">

           <h3 class='posts-titles  text-center'><?php echo $post['title']; ?></h3>
            
           <small class="post-date">Posted on: <?php echo $post['created_at']; ?> in <strong><?php echo $post ['name']; ?></strong> </small><br>
			<?php echo word_limiter($post['body'], 50); ?>
					
			<p><a class="btn btn-lg btn-danger" href="<?php echo site_url('/posts/'.$post['slug']); ?>">Read More</a>  </p>

			<img class="post-thumb col-md-11 offset-md-1 index-post-image" src="<?php echo site_url(); ?>assets/images/posts/<?php echo $post['post_image']; ?>">


        </div>

	</div>
</div> 	

<?php endforeach; ?> 

<br><br><br><br>
	

<div class="container-fluid">
    <div class="row text-center d-flex">
        <div class="row col-md-11 posts-blocks">

           	<div class="pagination-links">
				<?php echo $this->pagination->create_links(); ?>
			</div>
        </div>	  
	</div>
</div>
	

