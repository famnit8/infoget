
<h2 class="text-center all-titles"><strong><?= $title ?></strong></h2>


	<div class="row shadow p-3 mb-4 bg-body rounded col-md-10 about-blocks">
        <p> 
        	-Wondering where to find proper information about a certain topic? <br>-Want to share your thoughts?<br>-You found something interesting and you want to share it?
			<br><center>Share it with <strong>Info-Get</strong>.</center>
		</p>
    </div>

    <div class="row shadow p-3 mb-4 bg-body rounded col-md-10 about-blocks">
        <p>	
           Info-Get is a website where you can find and share information about any topic what so ever. You can create your own posts, your own topics and you have complete control by doing so. 
        </p>
    </div>

    <div class="row shadow p-3 mb-4 bg-body rounded col-md-10 about-blocks">
        <p> 
           Tutorial on how to use Info-Get: <a href="https://youtu.be/h2dOIpt5g1I"> InfoGet Tutorial </a>
        </p>
        <p> 
           GitLab link to the project: <a href="https://gitlab.com/famnit8/infoget"> GitLab Project </a>
        </p>
    </div>
        
         
       
   

