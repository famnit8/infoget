<h2 class="text-center read-more-title"><strong><?= $title ?></strong></h2>





<div class="container-fluid">
    <div class="row d-flex">
        <div class="row shadow p-3 mb-4 bg-body rounded col-md-11 categories-view-block">
			<ul class="list-group">
			<?php foreach($categories as $category) : ?>
				<li class="list-group-item"><a class="categories-text" href="<?php echo site_url('/categories/posts/'.$category['id']); ?>"><?php echo $category['name']; ?> </a>
					<?php if($this->session->userdata('user_id') == $category['user_id']): ?>
							
							<form class="cat-delete" action="categories/delete/<?php echo $category['id']; ?>" method="POST">
							
								<button type="submit" class="btn btn-lg btn-danger fa fa-close deleteButton"></button>
							</form>
					<?php endif; ?>
				</li>
			<?php endforeach; ?>
			</ul>


        </div>	  	
	</div>
</div>

