<h2 class="text-center read-more-title"><strong><?= $title ?></strong></h2>



<div class="container-fluid">
    <div class="row d-flex">
        <div class="row shadow p-3 mb-4 bg-body rounded col-md-11 categories-create-block">
				<?php echo validation_errors(); ?> 
			<?php echo form_open_multipart('categories/create');?> 
				<div class="form-group">
					<label>Name</label>
					<input type="text" class="form-control" name="name" placeholder="Enter name">
				</div> 
				<button type="submit" class="btn btn-lg btn-dark">Submit</button>
			</form>
        </div>	  
	</div>
</div>