<?php echo form_open('users/login'); ?>
	<div>
		<div class="col-md-5  sign-up">

			<h1 class="text-center"><?php echo $title; ?></h1>

			<div class="form-group">
				<label>Username</label>
				<input type="text" name="username" class="form-control" placeholder="Enter Username" required autofocus>
			</div>

			<div class="form-group">
				<label>Password</label>
				<input type="password" name="password" class="form-control" placeholder="Enter Password" required autofocus>
			</div>
			<div class="d-grid gap-2">
				<button type="submit" class="btn btn-lg btn-primary btn-block">Login</button>
			</div>
		</div>

	</div>

<?php echo form_close(); ?>